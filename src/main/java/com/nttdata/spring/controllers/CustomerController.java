package com.nttdata.spring.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nttdata.spring.repository.Customer;
import com.nttdata.spring.services.CustomerManagementServiceI;

/**
 * Controlador del cliente
 * @author Aaron
 *
 */

@Controller
@RequestMapping("/*")
public class CustomerController {
	
	@Autowired
	private CustomerManagementServiceI customerService;
	
	@RequestMapping(path = "/", method = RequestMethod.GET)
	public String listCustomers(Model model) {
		List<Customer> customerList = customerService.searchAll();
		model.addAttribute("customerList", customerList);
        model.addAttribute("newCustomer", new Customer());
         
		return "index";
	}
	
	@RequestMapping(path = "/search", method = RequestMethod.GET)
	public String getCustomerByName(@RequestParam String name, Model model) {
		List<Customer> customerList = customerService.searchByName(name);
		model.addAttribute("customerList", customerList);
		
		return "search";
	}
	
	@RequestMapping(path = "/delete", method = RequestMethod.GET)
	public String deleteCustomer(@RequestParam int id, Model model) {
		Customer deletedCustomer = customerService.findById(id);
		model.addAttribute("deletedCustomer", deletedCustomer);
		
		customerService.deleteById(id);

		return "delete";
	}
	
	@RequestMapping(path = "/add", method = RequestMethod.POST)
	public String insertNewCustomer(@ModelAttribute Customer newCustomer, Model model) {
		customerService.insertNewCustomer(newCustomer);
		model.addAttribute("newCustomer", newCustomer);
		
		return "add";
	}
	
}
